<?php

use yii\helpers\Html;

$this->title = $name;
?>

<div class="page-inner">
	<div class="page-title">
		<h3 class="breadcrumb-header"><?= $name ?></h3>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<p>
					<?= nl2br(Html::encode($message)) ?>
				</p>
			</div>
		</div>
	</div>
</div>

