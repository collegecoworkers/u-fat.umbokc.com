<!-- Page Sidebar -->
<div class="page-sidebar">
	<a class="logo-box" href="/admin/">
		<span>Space</span>
		<i class="icon-close" id="sidebar-toggle-button-close"></i>
	</a>
	<div class="page-sidebar-inner">
		<div class="page-sidebar-menu">
			<ul class="accordion-menu">
				<li>
					<a href="/">
						<i class="menu-icon icon-home4"></i><span>На сайт</span>
					</a>
				</li>
				<li>
					<a href="/admin/">
						<i class="menu-icon icon-home4"></i><span>Главная</span>
					</a>
				</li>
				<li>
					<a href="/admin/points">
						<i class="menu-icon icon-inbox"></i><span>Точки</span>
					</a>
				</li><!-- 
				<li>
					<a href="/admin/users">
						<i class="menu-icon icon-users"></i><span>Пользователи</span>
					</a>
				</li> -->
			</ul>
		</div>
	</div>
</div><!-- /Page Sidebar -->
