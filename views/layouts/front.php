<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

</body>
</html>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
