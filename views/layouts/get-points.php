<script>
	var placemarks = [
	<?php
	$r = "";
	foreach ($points as $item){
		$r .= "{latitude: {$item['latitude']}, longitude: {$item['longitude']},";
		$r .= "hintContent: '{$item['name']}', balloonContent: ['Автобуcы: ";
		if($item['data'] != null){
			$r .= "<table>";
			foreach ($item['data'] as $item){
				$r .= "<tr><th>{$item['name']}</th>";
				foreach ($item['data'] as $time){
					$r .= "<td>{$time}</td>";
				}
				$r .= "</tr>";
			}
			$r .= "</table>'";
		} else {
			$r .= "'";
		}
		$r .= "]},";
	}
	echo $r;
	?>
	];
</script>
