<?php
// use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Новая остановка';

?>

<!-- Page Inner -->
<div class="page-inner">
	<div class="page-title">
		<h3 class="breadcrumb-header"><?= $this->title ?></h3>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<form action="/admin/point" method="post">
							<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>"/
							<div class="form-group">
								<input type="text" class="form-control" name="Point[name]" placeholder="Название">
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<input type="text" class="form-control" name="Point[latitude]" placeholder="Широта">
								</div>
								<div class="form-group col-md-6">
									<input type="text" class="form-control" name="Point[longitude]" placeholder="Долгота">
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Сохранить</button>
							</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->render('../layouts/get-points', [
					'points' => $points
					]); ?>
				<div id="map"></div>
			</div>
		</div>
	</div>
</div>