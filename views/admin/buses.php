
<!-- Page Inner -->
<div class="page-inner">
  <div class="page-title">
    <a href="/admin/add-bus/?id=<?= $id ?>" class="btn btn-primary">Добавить автобус</a>
    <h3 class="breadcrumb-header">Данные</h3>
  </div>
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Автобус №</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table">
                <tbody>
                    <?php if ($buses != null): ?>
                      <?php foreach ($buses as $item): ?>
                        <tr>
                          <th>
                            <a href="/admin/bus-edit/?id=<?= $item['id'] ?>">
                              <i class="fa fa-pencil"></i>
                            </a>
                          </th>
                          <th><?= $item['name'] ?></th>
                          <?php foreach ($item['data'] as $val): ?>
                          <td><?= $val ?></td>
                          <?php endforeach ?>
                        </tr>
                      <?php endforeach ?>
                    <?php endif ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- Row -->
  </div><!-- Main Wrapper -->
</div><!-- /Page Inner -->
