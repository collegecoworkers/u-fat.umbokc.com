<?php
// use yii\helpers\Html;
// use yii\bootstrap\ActiveForm;

$this->title = 'Новый автобус';

?>

<!-- Page Inner -->
<div class="page-inner">
  <div class="page-title">
    <h3 class="breadcrumb-header"><?= $this->title ?></h3>
  </div>
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-body">
            <form action="/admin/add-bus/?id=<?= $id ?>" method="post">
              <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>"/>
              <div class="form-group">
                <input type="text" class="form-control" name="Bus[number]" placeholder="Номер">
              </div>
              <div class="form-group">
                <label for="">Время</label>
                <textarea class="form-control place" name="Bus[times]" value="" rows=10></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
