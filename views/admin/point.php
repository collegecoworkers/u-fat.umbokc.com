<?php
// use yii\helpers\Html;
// use yii\bootstrap\ActiveForm;

$this->title = $model->isNewRecord ? 'Новая остановка' : 'Изменить данные';

?>

<!-- Page Inner -->
<div class="page-inner">
	<div class="page-title">
		<h3 class="breadcrumb-header"><?= $this->title ?></h3>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<form id="w0" action="<?= $model->isNewRecord ? '/admin/point' : '/admin/point-edit/?id='.$model->id ?>" method="post">
							<div class="form-group">
								<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>"/>
								<input type="text" class="form-control" name="Point[name]" placeholder="Название"
								value="<?= $model->isNewRecord ? '' : $model->name ?>">
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<input type="text" class="form-control" name="Point[latitude]" placeholder="Широта"
									value="<?= $model->isNewRecord ? '' : $model->latitude ?>">
								</div>
								<div class="form-group col-md-6">
									<input type="text" class="form-control" name="Point[longitude]" placeholder="Долгота"
									value="<?= $model->isNewRecord ? '' : $model->longitude ?>">
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Сохранить</button>
							</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->render('../layouts/get-points', [
					'points' => $points
					]); ?>
				<div id="map"></div>
			</div>
		</div>
	</div>
</div>
