<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Page Container -->
<div class="page-container">
		<!-- Page Inner -->
		<div class="page-inner login-page" style="background: url('/img/bg-login.jpg') center center no-repeat fixed">
			<div id="main-wrapper" class="container-fluid">
				<div class="row">
					<div class="col-sm-6 col-md-3 login-box">
						<h4 class="login-title">Регистрация</h4>
						<?php $form = ActiveForm::begin(); ?>
							<div class="form-group">
								<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
							</div>
							<div class="form-group">
								<?= $form->field($model, 'name')->textInput() ?>
							</div>
							<div class="form-group">
								<?= $form->field($model, 'password')->passwordInput() ?>
							</div>
							<button type="submit" class="btn btn-primary">Регистрация</button>
							<a href="/auth/login" class="btn btn-default">Войти</a><br>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
	</div><!-- /Page Content -->
</div><!-- /Page Container -->