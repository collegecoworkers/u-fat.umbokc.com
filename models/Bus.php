<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Bus extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'bus';
	}

	public function rules()
	{
		return [
			[['name'], 'string'],
			[['data'], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'data' => 'Время',
		];
	}

	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	public static function getOfPoint($id)
	{
		$buses = [];
		$buses_ = self::find()->where(['point' => $id])->all();

		foreach ($buses_ as $bus) {
			$data = json_decode($bus->data);

			if($data != null){
				$the_val = [];
				$the_val['id'] = $bus->id;
				$the_val['name'] = $bus->name;
				foreach ($data as $key => $val){
					$the_val['data'][] = $val;
				}
				$buses[] = $the_val;
			}
		}

		return $buses;
	}

	public function getData() {
		$data = json_decode($this->data);
		$data = implode("\n", $data);
		return $data;
	}
}
