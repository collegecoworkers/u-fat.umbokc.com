<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Point extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'point';
	}

	public function rules()
	{
		return [
			[['latitude', 'longitude', 'name'], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'latitude' => 'Широта',
			'longitude' => 'Долгота',
		];
	}

	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	public static function getAll()
	{
		$points = [];

		$all = self::find()->all();

		foreach ($all as $item) {
			$buses = Bus::getOfPoint($item->id);

			$points[] = [
				'id' => $item->id,
				'name' => $item->name,
				'latitude' => $item->latitude,
				'longitude' => $item->longitude,
				'data' => $buses,
				'count' => count($buses),
			];
		}
		return $points;
	}

}
