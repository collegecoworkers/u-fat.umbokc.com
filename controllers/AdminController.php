<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\Point;
use app\models\Bus;

class AdminController extends Controller
{

	public function behaviors()
	{
		return [
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action)
	{
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}
		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		return $this->redirect('/admin/points');
	}

	public function actionPoints()
	{
		$points = Point::getAll();
		return $this->render('index', [
			'points' => $points,
		]);
	}

	public function actionPoint()
	{

		$model = new Point();

		if(Yii::$app->request->post()){
			$data = Yii::$app->request->post()['Point'];
			$model->name = $data['name'];
			$model->latitude = $data['latitude'];
			$model->longitude = $data['longitude'];
			$model->save();
			return $this->redirect('/admin/points');
		}

		return $this->render('point', [
			'model' => $model,
			'points' => Point::getAll(),
		]);
	}

	public function actionPointEdit($id)
	{

		$model = Point::findOne($id);

		if(Yii::$app->request->post()){
			$data = Yii::$app->request->post()['Point'];
			$model->name = $data['name'];
			$model->latitude = $data['latitude'];
			$model->longitude = $data['longitude'];
			$model->save();
			return $this->redirect('/admin/points');
		}

		return $this->render('point', [
			'model' => $model,
			'points' => Point::getAll(),
		]);
	}

	public function actionBuses($id)
	{
		$buses = Bus::getOfPoint($id);

		return $this->render('buses', [
			'buses' => $buses,
			'id' => $id,
		]);
	}

	public function actionAddBus($id)
	{

		if(Yii::$app->request->post()){
			$bus = Yii::$app->request->post()['Bus'];
			$model = new Bus();

			$times = explode("\n", $bus['times']);
			foreach ($times as &$item) $item = str_replace("\r", '', $item);

			$model->point = $id;
			$model->name = $bus['number'];
			$model->data = json_encode($times);
			$model->save();
			return $this->redirect('/admin/buses/?id=' . $id);
		}

		return $this->render('add-bus', [
			'id' => $id,
		]);
	}

	public function actionBusEdit($id)
	{

		$model = Bus::findOne($id);

		if(Yii::$app->request->post()){
			$bus = Yii::$app->request->post()['Bus'];

			$times = explode("\n", $bus['times']);
			foreach ($times as &$item) $item = str_replace("\r", '', $item);

			$model->name = $bus['number'];
			$model->data = json_encode($times);
			$model->save();
			return $this->redirect('/admin/buses/?id=' . $model->point);
		}

		return $this->render('edit-bus', [
			'model' => $model,
		]);
	}


}
