function isEmpty( el ){
	// return !$.trim(el.html())
	return el.length == 0;
}

function dbg(mes){
	console.log(mes);
	window.a = mes;
}

if(!isEmpty($('#map'))){
	ymaps.ready(init);

	var geoObjects= [];

	function init() {
		var myMap = new ymaps.Map('map', {
			center: [55.730, 37.9974],
			zoom: 13,
			controls: ['zoomControl'],
			behaviors: ['drag']
		}, {
			balloonMaxWidth: 200,
			searchControlProvider: 'yandex#search'
		});

		for (var i = 0; i < placemarks.length; i++) {
			var placemark = new ymaps.Placemark([placemarks[i].latitude, placemarks[i].longitude], {
				hintContent: placemarks[i].hintContent,
				balloonContent: placemarks[i].balloonContent.join(''),
				// iconContent: "Азербайджан"
			}, {
				preset: "twirl#yellowStretchyIcon",
				iconLayout: 'default#image',
				iconImageHref: '/img/travel.png',
				iconImageSize: [48, 48],
				iconImageOffset: [-24, -24],
			});

			myMap.geoObjects.add(placemark);
		}

		myMap.events.add('click', function (e) {
			if (!myMap.balloon.isOpen()) {
				var coords = e.get('coords');

				latitude = coords[0].toPrecision(5);
				longitude = coords[1].toPrecision(6);

				$('[name="Point[latitude]"]').val(latitude);
				$('[name="Point[longitude]"]').val(longitude);

				crds = [latitude, longitude].join(', ');

				myMap.balloon.open(coords, {
					contentHeader:'Сохранить эту точку?',
					contentBody:
					'<p>Координаты: ' + crds + '</p>'
						// '<a href="/site/save/?latitude=' + latitude + '&longitude=' + longitude + '">Сохранить</a>',
					});
			}
			else {
				myMap.balloon.close();
			}
		});

		// myMap.events.add('contextmenu', function (e) {
		// 	myMap.hint.open(e.get('coords'), 'Кто-то щелкнул правой кнопкой');
		// });

		// myMap.events.add('balloonopen', function (e) {
		// 	myMap.hint.close();
		// });
	}
}

var placeholder = '11:00\n12:15\n13:00\n14:15';
var text_area = $('.place[name="Bus[times]"]');

text_area.val(placeholder);

text_area.focus(function(){
	if($(this).val() === placeholder){
		$(this).val('');
	}
});

text_area.blur(function(){
	if($(this).val() ===''){
		$(this).val(placeholder);
	}
});
