ymaps.ready(init);

function dbg(mes){
	console.log(mes);
	window.a = mes;
}

var geoObjects= [];

function init() {
	var myMap = new ymaps.Map('map', {
		center: [55.730, 37.9974],
		zoom: 13,
		controls: ['zoomControl'],
		// behaviors: ['drag']
	}, {
		balloonMaxWidth: 200,
		searchControlProvider: 'yandex#search'
	});

	for (var i = 0; i < placemarks.length; i++) {
		var placemark = new ymaps.Placemark([placemarks[i].latitude, placemarks[i].longitude], {
			hintContent: placemarks[i].hintContent,
			balloonContent: placemarks[i].balloonContent.join(''),
			// iconContent: "Азербайджан"
		}, {
			preset: "twirl#yellowStretchyIcon",
			iconLayout: 'default#image',
			iconImageHref: '/img/travel.png',
			iconImageSize: [48, 48],
			iconImageOffset: [-24, -24],
		});

		myMap.geoObjects.add(placemark);
	}
}