<?php
use yii\db\Migration;

class m180202_074132_bus extends Migration
{

	public function up()
	{
		$this->createTable('bus', [
			'id' => $this->primaryKey(),
			'point'=>$this->integer(),
			'name'=>$this->string(),
			'data'=>$this->text(),
		]);
	}

	public function down()
	{
		$this->dropTable('user');
	}
}
