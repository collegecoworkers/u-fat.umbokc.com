<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'https://fonts.googleapis.com/css?family=Ubuntu',
        '/public/plugins/bootstrap/css/bootstrap.min.css',
        '/public/plugins/font-awesome/css/font-awesome.min.css',
        '/public/plugins/icomoon/style.css',
        '/public/plugins/uniform/css/default.css',
        '/public/plugins/switchery/switchery.min.css',
        '/public/css/space.min.css',
        '/public/css/custom.css',
    ];
    public $js = [
        '/public/plugins/jquery/jquery-3.1.0.min.js',
        '/public/plugins/bootstrap/js/bootstrap.min.js',
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        '/js/admin.js',
        '/public/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        '/public/plugins/uniform/js/jquery.uniform.standalone.js',
        '/public/plugins/switchery/switchery.min.js',
        '/public/js/space.min.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
